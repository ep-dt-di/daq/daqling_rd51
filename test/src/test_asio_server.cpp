#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <chrono>

using boost::asio::ip::udp;
using boost::asio::ip::address;

#include "Utilities/Logging.hpp"

int main() {
  using namespace std::chrono_literals;

  boost::asio::io_service io_service;
  udp::socket socket(io_service, udp::endpoint(udp::v4(), 6006));
  socket.non_blocking(true);
  
  boost::system::error_code error;
  int i = 0;
  while (i<100) {  
    boost::array<char, 9000> recv_buf;
    udp::endpoint remote_endpoint(address::from_string("127.0.0.1"), 6006);
    size_t len = socket.receive_from(boost::asio::buffer(recv_buf), remote_endpoint, 0, error);
    if (error == boost::asio::error::would_block) {
      std::this_thread::sleep_for(100us);
    } else {
      INFO("Received message " << ++i << " | len: " << len);
    }
  }
}