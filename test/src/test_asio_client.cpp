#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <pcap.h>

using boost::asio::ip::udp;
using boost::asio::ip::address;

#include "Utilities/Logging.hpp"

int main(int argc, char * argv[]) {
  if (argc == 1) {
    ERROR("Specify the .pcapng file to replay");
    return 1;
  }
  boost::asio::io_service io_service;
  udp::endpoint receiver_endpoint(address::from_string("127.0.0.1"), 6006);
  udp::socket socket(io_service);
  socket.open(udp::v4());

  std::string file(argv[1]);
  char errbuff[PCAP_ERRBUF_SIZE];
  pcap_t * pcap = pcap_open_offline(file.c_str(), errbuff);
  struct pcap_pkthdr *header;
  const u_char *data;
  u_int packetCount = 0;
  while (int returnValue = pcap_next_ex(pcap, &header, &data) >= 0)
  {
    INFO("Packet # " << ++packetCount);
    INFO("Packet size: " << header->len << " bytes");
    if (header->len != header->caplen)
      WARNING("Capture size different than packet size: "<<header->len<<" bytes");
    INFO("Epoch Time: "<<header->ts.tv_sec<<":"<<header->ts.tv_usec<<" seconds");
    // for (u_int i=0; (i < header->caplen ) ; i++)
    // {
    //   if ( (i % 16) == 0) printf("\n");
    //   printf("%.2x ", data[i]);
    // }
    size_t len =  socket.send_to(boost::asio::buffer(data+42, header->caplen - 42), receiver_endpoint);
    INFO("\n\n");
  }
  return 0;
}