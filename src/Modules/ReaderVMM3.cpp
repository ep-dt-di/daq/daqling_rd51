/**
 * Copyright (C) 2019 CERN
 *
 * DAQling is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DAQling is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DAQling. If not, see <http://www.gnu.org/licenses/>.
 */

#include <chrono>
#include <fstream>

#include "Modules/ParserVMM3.h"
#include "Modules/ReaderVMM3.hpp"

using namespace std::chrono_literals;

extern "C" ReaderVMM3 *create_object() { return new ReaderVMM3; }
extern "C" void destroy_object(ReaderVMM3 *object) { delete object; }

ReaderVMM3::ReaderVMM3() {
  INFO("ReaderVMM3::ReaderVMM3");
  m_socket = std::make_unique<udp::socket>(m_io_service, udp::endpoint(udp::v4(), 6006));
  m_socket->non_blocking(true);
  m_remote_endpoint = std::make_unique<udp::endpoint>(address::from_string("10.0.0.3"), 6006);

  m_packet_queue = std::make_unique<jumbo_queue_t>(1000000);
}

ReaderVMM3::~ReaderVMM3() { INFO("ReaderVMM3::~ReaderVMM3"); }

void ReaderVMM3::start() {
  daqling::core::DAQProcess::start();
  m_parser_thread = std::make_unique<std::thread>(&ReaderVMM3::parser, this);
  INFO("ReaderVMM3::start");
}

void ReaderVMM3::stop() {
  daqling::core::DAQProcess::stop();
  m_parser_thread->join();
  INFO("ReaderVMM3::stop");
}

void ReaderVMM3::runner() {
  INFO(" Running...");
  int i = 0;
  boost::system::error_code error;
  while (m_run) {
    jumbo_size_t recv_buf;
    recv_buf.second =
        m_socket->receive_from(boost::asio::buffer(recv_buf.first), *m_remote_endpoint, 0, error);
    if (error == boost::asio::error::would_block) {
      std::this_thread::sleep_for(100us);
    } else {
      INFO("Received message " << ++i << " | len: " << recv_buf.second);
      bool rv = m_packet_queue->write(recv_buf);
    }
  }
  INFO(" Runner stopped");
}

void ReaderVMM3::parser() {
  Gem::VMM3SRSData parser(1500);
  jumbo_size_t udp_packet;
  std::ofstream csv_file;
  csv_file.open("/tmp/readouts.csv");

  while (m_run) {
    if (!m_packet_queue->isEmpty()) {
      bool rv = m_packet_queue->read(udp_packet);
      parser.receive(udp_packet.first.data(), udp_packet.second);
      INFO("Readouts after parse: " << parser.stats.readouts);

      // field fec id starts at 1
      Readout readout;
      readout.fec = parser.parserData.fecId;
      for (unsigned int i = 0; i < parser.stats.readouts; i++) {
        auto &d = parser.data[i];
        if (d.hasDataMarker) {
          // \todo should these be functions of SRSTime?
          readout.srs_timestamp =
              (static_cast<uint64_t>(d.fecTimeStamp) * Gem::SRSTime::internal_SRS_clock_period_ns +
               static_cast<uint64_t>(d.triggerOffset) * time_intepreter_.trigger_period_ns());

          readout.chip_id = d.vmmid;
          readout.channel = d.chno;
          readout.bcid = d.bcid;
          readout.tdc = d.tdc;
          readout.adc = d.adc;
          readout.over_threshold = (d.overThreshold != 0);
          // auto calib = calfile_->getCalibration(readout.fec, readout.chip_id, readout.channel);
          // \todo does this really need to be a floating point value?
          // readout.chiptime = static_cast<float>(
              // time_intepreter_.chip_time_ns(d.bcid, d.tdc, calib.offset, calib.slope));

          // No calibration
          readout.chiptime = static_cast<float>(
              time_intepreter_.chip_time_ns(d.bcid, d.tdc, 0, 1));

          // \todo what if chiptime is negative?

          csv_file << (int)readout.fec << "," << (int)readout.chip_id << "," << readout.srs_timestamp << "," 
              << readout.channel << ","  << readout.bcid << "," << readout.tdc << "," 
              << readout.adc << "," << readout.over_threshold << "," << readout.chiptime << "," << "\n";

          char msg[256];
          sprintf(msg, "srs/vmm timestamp: srs: 0x%08x, bc: 0x%08x, tdc: 0x%08x",
                  readout.srs_timestamp, d.bcid, d.tdc);
          DEBUG(msg);
          sprintf(msg, "srs/vmm chip: %d, channel: %d", readout.chip_id, d.chno);
          DEBUG(msg);
        }
      }
    } else {
      std::this_thread::sleep_for(100us);
    }
  }
  csv_file.close();
}