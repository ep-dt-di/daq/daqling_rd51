/** Copyright (C) 2018 European Spallation Source ERIC */

#include <arpa/inet.h>
#include <cinttypes>
#include <cstdio>
#include <string.h>

#include "Modules/ParserVMM3.h"

namespace Gem {

int VMM3SRSData::parse(uint32_t data1, uint16_t data2, struct VMM3Data *vmd) {

  char msg[256];
  sprintf(msg, "data1: 0x%08x, data2: 0x%04x", data1, data2);
  DEBUG(msg);

  int dataflag = (data2 >> 15) & 0x1;

  if (dataflag) {
    /// Data
    sprintf(msg, "SRS Data");
    DEBUG(msg);

    vmd->overThreshold = (data2 >> 14) & 0x01;
    vmd->chno = (data2 >> 8) & 0x3f;
    vmd->tdc = data2 & 0xff;

    vmd->vmmid = (data1 >> 22) & 0x1F;
    vmd->triggerOffset = (data1 >> 27) & 0x1F;
    vmd->adc = (data1 >> 12) & 0x3FF;
    vmd->bcid = BitMath::gray2bin32(data1 & 0xFFF);

    vmd->fecTimeStamp = markers[(parserData.fecId - 1) * maximumNumberVMM + vmd->vmmid].fecTimeStamp;
    if (markers[(parserData.fecId - 1) * maximumNumberVMM + vmd->vmmid].fecTimeStamp > 0) {
      vmd->hasDataMarker = true;
    }
    return 1;
  } else {
    /// Marker
    uint8_t vmmid = (data2 >> 10) & 0x1F;
    uint64_t timestamp_lower_10bit = data2 & 0x03FF;
    uint64_t timestamp_upper_32bit = data1;

    uint64_t timestamp_42bit = (timestamp_upper_32bit << 10)
        + timestamp_lower_10bit;
    sprintf(msg, "SRS Marker vmmid %d: timestamp lower 10bit %u, timestamp upper 32 bit %u, 42 bit timestamp %"
        PRIu64
        "", vmmid, timestamp_lower_10bit, timestamp_upper_32bit, timestamp_42bit);
    DEBUG(msg);
    markers[(parserData.fecId - 1) * maximumNumberVMM + vmmid].fecTimeStamp = timestamp_42bit;

    sprintf(msg, "vmmid: %d", vmmid);
    DEBUG(msg);
    return 0;
  }
}

int VMM3SRSData::receive(const char *buffer, int size) {
  char msg[256];
  memset(&stats, 0, sizeof(stats));

  if (size < 4) {
    sprintf(msg,  "Undersize data");
    DEBUG(msg);
    stats.errors += size;
    return 0;
  }

  struct SRSHdr *srsHeaderPtr = (struct SRSHdr *) buffer;
  srsHeader.frameCounter = ntohl(srsHeaderPtr->frameCounter);
  if (srsHeader.frameCounter == 0xfafafafa) {
    sprintf(msg, "End of Frame");
    INFO(msg);
    stats.badFrames++;
    stats.errors += size;
    return -1;
  }

  if (parserData.nextFrameCounter != srsHeader.frameCounter) {
    sprintf(msg, "FC error %u != %u", parserData.nextFrameCounter, srsHeader.frameCounter);
    WARNING(msg);
    stats.rxSeqErrors++;
  }
  parserData.nextFrameCounter = srsHeader.frameCounter + 1;

  if (size < SRSHeaderSize + HitAndMarkerSize) {
    sprintf(msg, "Undersize data");
    WARNING(msg);
    stats.badFrames++;
    stats.errors += size;
    return 0;
  }

  srsHeader.dataId = ntohl(srsHeaderPtr->dataId);
  /// maybe add a protocol error counter here
  if ((srsHeader.dataId & 0xffffff00) != 0x564d3300) {
    sprintf(msg, "Unknown data");
    WARNING(msg);
    stats.badFrames++;
    stats.errors += size;
    return 0;
  }

  parserData.fecId = (srsHeader.dataId >> 4) & 0x0f;
  if (parserData.fecId < 1 || parserData.fecId > 15) {
    sprintf(msg, "Invalid fecId: %u", parserData.fecId);
    WARNING(msg);
    stats.badFrames++;
    stats.errors += size;
    return 0;
  }
  srsHeader.udpTimeStamp = ntohl(srsHeaderPtr->udpTimeStamp);
  srsHeader.offsetOverflow = ntohl(srsHeaderPtr->offsetOverflow);

  auto datalen = size - SRSHeaderSize;
  if ((datalen % 6) != 0) {
    sprintf(msg, "Invalid data length: %d", datalen);
    WARNING(msg);
    stats.badFrames++;
    stats.errors += size;
    return 0;
  }

  // XTRACE(PROCESS, DEB, "VMM3a Data, VMM Id %d", vmmid);

  int dataIndex = 0;
  int readoutIndex = 0;
  while (datalen >= HitAndMarkerSize) {
    sprintf(msg, "readoutIndex: %d, datalen %d, elems: %u",
    		readoutIndex, datalen, stats.readouts);
    DEBUG(msg);
    auto Data1Offset = SRSHeaderSize + HitAndMarkerSize * readoutIndex;
    auto Data2Offset = Data1Offset + Data1Size;
    uint32_t data1 = htonl(*(uint32_t *) &buffer[Data1Offset]);
    uint16_t data2 = htons(*(uint16_t *) &buffer[Data2Offset]);

    int res = parse(data1, data2, &data[dataIndex]);
    if (res == 1) { // This was data
      stats.readouts++;
      dataIndex++;
    } else {
      stats.markers++;
    }
    readoutIndex++;

    datalen -= 6;
    if (stats.readouts == maxHits && datalen > 0) {
      sprintf(msg, "Data overflow, skipping %d bytes", datalen);
      INFO(msg);
      stats.errors += datalen;
      break;
    }
  }
  stats.goodFrames++;

  return stats.readouts;
}

}
