/**
 * Copyright (C) 2019 CERN
 * 
 * DAQling is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DAQling is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with DAQling. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DAQLING_MODULES_READERVMM3_HPP
#define DAQLING_MODULES_READERVMM3_HPP

#include <boost/asio.hpp>
#include <boost/array.hpp>

#include "Core/DAQProcess.hpp"
// #include "Modules/CalibrationFile.h"
#include "Modules/SRSTime.h"
#include "Utilities/ProducerConsumerQueue.hpp"

using boost::asio::ip::udp;
using boost::asio::ip::address;

class ReaderVMM3 : public daqling::core::DAQProcess {
 private:
  typedef boost::array<char, 9000> udp_jumbo_t;
  typedef std::pair<udp_jumbo_t, int> jumbo_size_t;
  typedef folly::ProducerConsumerQueue<jumbo_size_t> jumbo_queue_t;

  boost::asio::io_service m_io_service;
  std::unique_ptr<udp::socket> m_socket;
  std::unique_ptr<udp::endpoint> m_remote_endpoint;
  std::unique_ptr<std::thread> m_parser_thread;
  std::unique_ptr<jumbo_queue_t> m_packet_queue;

  // std::shared_ptr<Gem::CalibrationFile> calfile_;
  Gem::SRSTime time_intepreter_;

  void parser();

 public:
  ReaderVMM3();
  ~ReaderVMM3();

  void start();
  void stop();

  void runner();
};

struct __attribute__ ((packed)) Readout {
  /// \todo use constexpr string_view when c++17 arrives
  static std::string DatasetName() { return "srs_hits"; }
  static uint16_t FormatVersion() { return 1; }

  /// \todo consider reordering these to optimize
  /// !!! DO NOT MODIFY BELOW - READ HEADER FIRST !!!
  uint8_t fec{0};
  uint8_t chip_id{0};
  uint64_t srs_timestamp{0};
  uint16_t channel{0};
  uint16_t bcid{0};
  uint16_t tdc{0};
  uint16_t adc{0};
  bool over_threshold{false};
  float chiptime{0.0};
  /// !!! DO NOT MODIFY ABOVE -- READ HEADER FIRST !!!

  bool operator==(const Readout &other) const {
    return (
        (fec == other.fec) &&
            (chip_id == other.chip_id) &&
            (srs_timestamp == other.srs_timestamp) &&
            (channel == other.channel) &&
            (bcid == other.bcid) &&
            (tdc == other.tdc) && (adc == other.adc) &&
            (over_threshold == other.over_threshold) &&
            (chiptime == other.chiptime)
    );
  }
};

#endif // DAQLING_MODULES_READERVMM3_HPP
